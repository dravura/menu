<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Crud ejercicio</title>
        
        <meta charset="utf-8" />
        
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="assets/js/jquery-ui/jquery-ui.min.css" />
        <link rel="stylesheet" href="assets/css/style.css" />
        <link href="plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
         <script src="plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
          <style>
   body
   {
    margin:0;
    padding:0;
    background-color:#f1f1f1;
   }
   .box
   {
    width:1270px;
    padding:20px;
    background-color:#fff;
    border:1px solid #ccc;
    border-radius:5px;
    margin-top:25px;
   }

   .dropdown > .dropdown-menu {
  display: none; //hides .dropdown-menu directly inside .dropdown
}
.dropdown.open > .dropdown-menu {
  display: block; //shows .dropdown-menu directly inside .dropdown if it has class .open
}
  </style>
	</head>
    <body>
        
  <div class="container box">
  <!--Pinto el menu en caso de que haya datos-->
  <?php 
  $dataMenu=$this->model->Menus();
  if($dataMenu)
  {
    ?>
    <ul class="nav nav-tabs"> 
    <?php
    foreach($dataMenu as $m):
      ?>
        
          <?php
          $submenu = $this->model->Submenus($m->id); 
          if($submenu):
          ?>
            <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <?php echo $m->menu;?> <span class="caret"></span> </a> 
              <ul class="dropdown-menu"> 
                <?php 
                foreach($submenu as $s):
                ?>
                <li><a href="#"><?php echo $s->menu;?></a></li> 
              <?php endforeach;?>
              </ul> 
            </li> 
          <?php 
          else:
            ?>
              <li role="presentation"><a href="#"><?php echo $m->menu;?></a></li> 
            <?php
          endif;?>          
      <?php

    endforeach;
    ?>
    </ul> 
    <?php
  }
  else
  {
    echo "Aun no se han ingresado datos al menu";
  }
   ?>
    