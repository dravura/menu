<h1 class="page-header">CRUD </h1>


    <a class="btn btn-primary pull-right" href="?c=menu&a=Crud">Agregar</a>
<br><br><br>

<table class="table  table-striped  table-hover" id="tabla">
    <thead>
        <tr>
        <th style="width:120px; background-color: #5DACCD; color:#fff">ID</th>
            <th style="width:180px; background-color: #5DACCD; color:#fff">Nombre</th>
            <th style=" background-color: #5DACCD; color:#fff">Menu padre</th>
            <th style=" background-color: #5DACCD; color:#fff">Descripcion</th>
            <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
            <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
         <td><?php echo $r->id; ?></td>
            <td><?php echo $r->menu; ?></td>
            <td><?php 
                if($r->parent_id>0)
                {
                    echo $this->model->Menu($r->parent_id);
                }
             ?></td>
            <td><?php echo $r->descripcion; ?></td>
            <td>
                <a  class="btn btn-warning" href="?c=menu&a=Crud&id=<?php echo $r->id; ?>">Editar</a>
            </td>
            <td>
                <a  class="btn btn-danger" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=menu&a=Eliminar&id=<?php echo $r->id; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 

</body>
<script  src="assets/js/datatable.js">  

</script>


</html>
