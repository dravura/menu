<h1 class="page-header">
    <?php echo $menu->id != null ? $menu->menu : 'Nuevo Registro'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=menu">Menu</a></li>
  <li class="active"><?php echo $menu->id != null ? $menu->menu : 'Nuevo Menu'; ?></li>
</ol>

<form id="crud" action="?c=menu&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $menu->id; ?>" />

    <div class="form-group">
        <label>Menu padre</label>
        <select name="parent_id" class="form-control">
            <option value=0>Es menú padre</option>
            <?php foreach($this->model->Menus() as $m): ?>
                <option value=<?php echo $m->id; ?>>
                <?php echo $m->menu; ?>
                </option>
            <?php endforeach;?>
        </select>
    </div>
      <div class="form-group">
        <label>Nombre menú</label>
        <input type="text" name="menu" value="<?php echo $menu->menu; ?>" class="form-control" placeholder="Ingrese el nombre del menu" required>
    </div>
    
    <div class="form-group">
        <label>Descripción</label>
        <input type="text" name="descripcion" value="<?php echo $menu->descripcion; ?>" class="form-control" placeholder="Ingrese la descripcion" required>
    </div>
    
    <hr />
    
    <div class="text-right">
        <button class="btn btn-primary">Guardar</button>
        <a href="index.php" id="cancel" name="cancel" class="btn btn-danger">Cancelar</a>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#crud").submit(function(){
            return $(this).validate();
        });
    })
</script>