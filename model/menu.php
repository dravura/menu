<?php
class menu
{
	private $pdo;
    
    public $id;
    public $menu;
    public $descripcion;
    public $parent_id;  

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM menu");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Menu($parent_id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM menu WHERE id = ?");
			          

			$stm->execute(array($parent_id));
			return $stm->fetch(PDO::FETCH_OBJ)->menu;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Menus()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM menu where parent_id = 0"); //Solo muestro los padres solo un nivel
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Obtener($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM menu WHERE id = ?");
			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($id)
	{
		try 
		{
			$stm = $this->pdo
			            ->prepare("DELETE FROM menu WHERE id = ?");			          

			$stm->execute(array($id));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Actualizar($data)
	{
		try 
		{
			$sql = "UPDATE menu SET 
						menu      		= ?,
						descripcion          = ?, 
						parent_id        = ?						
				    WHERE id = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
				    	$data->menu, 
                        $data->descripcion,                        
                        $data->paren_id,
                        $data->id
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Registrar(menu $data)
	{
		try 
		{
		$sql = "INSERT INTO menu (menu, descripcion, parent_id) 
		        VALUES (?, ?, ?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
					 $data->menu, 
                    $data->descripcion,
                    $data->parent_id
                )
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Submenus($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM menu WHERE parent_id = ?");
			$stm->execute(array($id));
			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}