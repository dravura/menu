<?php
require_once 'model/menu.php';

class menuController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new menu();
    }
    
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/menu/menu.php';
       
    }
    
    public function Crud(){
        $menu = new menu();
        
        if(isset($_REQUEST['id'])){
            $menu = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'view/header.php';
        require_once 'view/menu/menu-editar.php';
        
    }
    
    public function Guardar(){
        $menu = new menu();
        
        $menu->id = $_REQUEST['id'];
        $menu->menu = $_REQUEST['menu'];
        $menu->descripcion = $_REQUEST['descripcion'];
        $menu->parent_id = $_REQUEST['parent_id'];  

        if($menu->id > 0)
        {
            $this->model->Actualizar($menu);
        } 
        else
        {
            $this->model->Registrar($menu);
        }
        
        header('Location: index.php');
    }
    


    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php');
    }
}